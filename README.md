# pvaPy conda recipe

Home: https://github.com/epics-base/pvaPy

Package license: EPICS Open License

Recipe license: BSD 2-Clause

Summary: Python bindings for EPICS pvAccess
